require 'test_helper'

class AirbusesControllerTest < ActionController::TestCase
  setup do
    @airbus = airbuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:airbuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create airbus" do
    assert_difference('Airbus.count') do
      post :create, airbus: { status: @airbus.status }
    end

    assert_redirected_to airbus_path(assigns(:airbus))
  end

  test "should show airbus" do
    get :show, id: @airbus
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @airbus
    assert_response :success
  end

  test "should update airbus" do
    patch :update, id: @airbus, airbus: { status: @airbus.status }
    assert_redirected_to airbus_path(assigns(:airbus))
  end

  test "should destroy airbus" do
    assert_difference('Airbus.count', -1) do
      delete :destroy, id: @airbus
    end

    assert_redirected_to airbuses_path
  end
end
