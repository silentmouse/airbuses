class AirbusWorker

  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper

  def perform(*args)
    while $redis.get("block") == "1"
      sleep 1
    end
    $redis.set("block","1")
    10.times do |t|
      sleep 1
      puts t
    end
    airbus = Airbus.find(args[0])
    airbus.update_column :status, 2
    Event.create(airbus_id: args[0],change_status: 2, updated: 0)
    puts "Update #{args[0]} Time = #{Time.now}"
    $redis.set("block","0")
  end

end
