json.extract! airbus, :id, :status, :created_at, :updated_at
json.url airbus_url(airbus, format: :json)