class AirbusesController < ApplicationController
  before_action :set_airbus, only: [:show, :edit, :update, :destroy]

  # GET /airbuses
  # GET /airbuses.json
  def index
    @airbuses = Airbus.all
  end

  # GET /airbuses/1
  # GET /airbuses/1.json
  def show
    @events = @airbus.events
  end

  # GET /airbuses/new
  def new
    @airbus = Airbus.new
  end

  # GET /airbuses/1/edit
  def edit
  end

  # POST /airbuses
  # POST /airbuses.json
  def create
    @airbus = Airbus.new(airbus_params)

    respond_to do |format|
      if @airbus.save
        format.html { redirect_to @airbus, notice: 'Airbus was successfully created.' }
        format.json { render :show, status: :created, location: @airbus }
      else
        format.html { render :new }
        format.json { render json: @airbus.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /airbuses/1
  # PATCH/PUT /airbuses/1.json
  def update
    respond_to do |format|
      if @airbus.update(airbus_params)
        format.html { redirect_to @airbus, notice: 'Airbus was successfully updated.' }
        format.json { render :show, status: :ok, location: @airbus }
      else
        format.html { render :edit }
        format.json { render json: @airbus.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /airbuses/1
  # DELETE /airbuses/1.json
  def destroy
    @airbus.destroy
    respond_to do |format|
      format.html { redirect_to airbuses_url, notice: 'Airbus was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def takeoff
    Airbus.find(params[:id]).update_column :status, 1
    job_id = AirbusWorker.perform_async(params[:id])
    Event.create(change_status: 1, airbus_id: params[:id],updated: 1)
    render json: {job_id: job_id}
  end

  def landing
    Airbus.find(params[:id]).update_column :status, 0
    Event.create(change_status: 0, airbus_id: params[:id],updated: 1)
    render json: {success: "ok"}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_airbus
      @airbus = Airbus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def airbus_params
      params.require(:airbus).permit(:status)
    end
end
