class JobsController < ApplicationController

  def index
    event = Event.where(updated: 0).first
    if event.present?
      event.update_column :updated, 1
      render json: {data: [event.airbus_id]}
    else
      render json: {data: ""}
    end
  end

end
