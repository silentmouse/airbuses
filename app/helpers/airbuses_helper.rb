module AirbusesHelper

  def get_status_air(status)
    case status
      when 0
        "<span style='color:black'>В ангаре</span>".html_safe
      when 1
        "<span style='color:#dbb520'>Одидание</span>".html_safe
      when 2
        "<span style='color:green'>В воздухе</span>".html_safe
    end
  end



end
