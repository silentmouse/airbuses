class CreateAirbuses < ActiveRecord::Migration
  def change
    create_table :airbuses do |t|
      t.integer :status

      t.timestamps null: false
    end
  end
end
