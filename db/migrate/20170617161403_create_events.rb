class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :airbus_id
      t.integer :change_status
      t.integer :updated

      t.timestamps null: false
    end
  end
end
